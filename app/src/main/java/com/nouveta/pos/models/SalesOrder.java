
package com.nouveta.pos.models;


public class SalesOrder {

    private String orderId;
    private String orderNumber;
    private String orderAmount;
    private String paymentStatus;
    private String userCode;
    private String clearenceStatus;
    private String date;
    private String businessCode;

    /**
     * No args constructor for use in serialization
     * 
     */
    public SalesOrder() {
    }

    /**
     * 
     * @param clearenceStatus
     * @param userCode
     * @param orderAmount
     * @param businessCode
     * @param orderNumber
     * @param paymentStatus
     * @param date
     * @param orderId
     */
    public SalesOrder(String orderId, String orderNumber, String orderAmount, String paymentStatus, String userCode, String clearenceStatus, String date, String businessCode) {
        super();
        this.orderId = orderId;
        this.orderNumber = orderNumber;
        this.orderAmount = orderAmount;
        this.paymentStatus = paymentStatus;
        this.userCode = userCode;
        this.clearenceStatus = clearenceStatus;
        this.date = date;
        this.businessCode = businessCode;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getClearenceStatus() {
        return clearenceStatus;
    }

    public void setClearenceStatus(String clearenceStatus) {
        this.clearenceStatus = clearenceStatus;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

}
