
package com.nouveta.pos.models;


public class SubCategory {

    private String id;
    private String categoryId;
    private String subCategoryId;
    private String subCategoryName;
    private String subCategoryImage;
    private String subCategoryDescription;
    private String businessCode;
    private String item_amount;

    /**
     * No args constructor for use in serialization
     * 
     */
    public SubCategory() {
    }

    /**
     * 
     * @param subCategoryId
     * @param id
     * @param subCategoryImage
     * @param businessCode
     * @param categoryId
     * @param subCategoryDescription
     * @param subCategoryName
     */
    public SubCategory(String id, String categoryId, String subCategoryId, String subCategoryName, String subCategoryImage, String subCategoryDescription, String businessCode,String item_amount) {
        super();
        this.id = id;
        this.categoryId = categoryId;
        this.subCategoryId = subCategoryId;
        this.subCategoryName = subCategoryName;
        this.subCategoryImage = subCategoryImage;
        this.subCategoryDescription = subCategoryDescription;
        this.businessCode = businessCode;
        this.item_amount = item_amount;
    }

    public String getItem_amount() {
        return item_amount;
    }

    public void setItem_amount(String item_amount) {
        this.item_amount = item_amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getSubCategoryImage() {
        return subCategoryImage;
    }

    public void setSubCategoryImage(String subCategoryImage) {
        this.subCategoryImage = subCategoryImage;
    }

    public String getSubCategoryDescription() {
        return subCategoryDescription;
    }

    public void setSubCategoryDescription(String subCategoryDescription) {
        this.subCategoryDescription = subCategoryDescription;
    }

    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

}
