
package com.nouveta.pos.models;


public class Sales {

    private String salesId;
    private String orderNumber;
    private String subCategoryId;
    private String itemAmount;
    private String itemQuantity;
    private String totalItemAmount;
    private String userCode;
    private String date;
    private String businessCode;
    private String sub_category_name;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Sales() {
    }

    /**
     * 
     * @param subCategoryId
     * @param itemQuantity
     * @param userCode
     * @param businessCode
     * @param itemAmount
     * @param orderNumber
     * @param totalItemAmount
     * @param salesId
     * @param date
     */
    public Sales(String salesId, String orderNumber, String subCategoryId, String itemAmount, String itemQuantity, String totalItemAmount, String userCode, String date, String businessCode,String sub_category_name) {
        super();
        this.salesId = salesId;
        this.orderNumber = orderNumber;
        this.subCategoryId = subCategoryId;
        this.itemAmount = itemAmount;
        this.itemQuantity = itemQuantity;
        this.totalItemAmount = totalItemAmount;
        this.userCode = userCode;
        this.date = date;
        this.businessCode = businessCode;
        this.sub_category_name = sub_category_name;
    }

    public void setSub_category_name(String sub_category_name) {
        this.sub_category_name = sub_category_name;
    }

    public String getSub_category_name() {
        return sub_category_name;
    }

    public String getSalesId() {
        return salesId;
    }

    public void setSalesId(String salesId) {
        this.salesId = salesId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(String itemAmount) {
        this.itemAmount = itemAmount;
    }

    public String getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(String itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public String getTotalItemAmount() {
        return totalItemAmount;
    }

    public void setTotalItemAmount(String totalItemAmount) {
        this.totalItemAmount = totalItemAmount;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

}
