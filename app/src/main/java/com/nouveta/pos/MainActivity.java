package com.nouveta.pos;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.nouveta.pos.adapter.Adapter;
import com.nouveta.pos.adapter.AdapterSales;
import com.nouveta.pos.adapter.AdapterSalesOrder;
import com.nouveta.pos.adapter.AdapterSubCategory;
import com.nouveta.pos.api.API;
import com.nouveta.pos.models.Category;
import com.nouveta.pos.models.Sales;
import com.nouveta.pos.models.SalesOrder;
import com.nouveta.pos.models.SubCategory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import test.jinesh.easypermissionslib.EasyPermission;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,EasyPermission.OnPermissionResult {

    RecyclerView recyclerViewH;
    RecyclerView.Adapter mAdapterH;
    RecyclerView.LayoutManager mLayoutManagerH;

    RecyclerView recyclerViewV1;
    RecyclerView.Adapter mAdapterV1;
    RecyclerView.LayoutManager mLayoutManagerV1;

    RecyclerView recyclerViewV2;
    RecyclerView.Adapter mAdapterV2;
    RecyclerView.LayoutManager mLayoutManagerV2;

    RecyclerView recyclerViewV3;
    RecyclerView.Adapter mAdapterV3;
    RecyclerView.LayoutManager mLayoutManagerV3;

    ProgressBar progressBar,progressBar2,progressBar3;

    SwipeRefreshLayout   swiperefresh;
    LinearLayout details;

    TextView title,title2,title3,createOrder,orderCompleted,tvorder;

    public  String order_number ="";

    String mydate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());

    String food ="";

    EasyPermission easyPermission;
    public static String business_code = "1";
    EditText phone,name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        easyPermission = new EasyPermission();
        easyPermission.requestPermission(this, Manifest.permission.ACCESS_NETWORK_STATE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        title = (TextView) findViewById(R.id.title);
        title2 = (TextView) findViewById(R.id.title2);
        title3 = (TextView) findViewById(R.id.title3);
        tvorder = (TextView) findViewById(R.id.tvorder);
        details = (LinearLayout) findViewById(R.id.details);

        phone = (EditText)findViewById(R.id.phone);
        name = (EditText) findViewById(R.id.name);


        createOrder = (TextView) findViewById(R.id.createOrder);

        createOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createSalesOrder("4455", "1");
                Snackbar.make(v, "Creating Order", Snackbar.LENGTH_LONG)
                        .setAction("Orders", null).show();
            }
        });
        orderCompleted =  (TextView) findViewById(R.id.orderCompleted);
        orderCompleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                completeOrder(order_number);
            }
        });

        recyclerViewH = (RecyclerView) findViewById(R.id.recyclerViewH);
        recyclerViewH.setHasFixedSize(true);
        mLayoutManagerH =new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewH.setLayoutManager(mLayoutManagerH);


        recyclerViewV1 = (RecyclerView) findViewById(R.id.recyclerViewV1);
        recyclerViewV1.setHasFixedSize(true);
        mLayoutManagerV1 =new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewV1.setLayoutManager(mLayoutManagerV1);

        recyclerViewV2 = (RecyclerView) findViewById(R.id.recyclerViewV2);
        recyclerViewV2.setHasFixedSize(true);
        mLayoutManagerV2 =new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewV2.setLayoutManager(mLayoutManagerV2);

        recyclerViewV3 = (RecyclerView) findViewById(R.id.recyclerViewV3);
        recyclerViewV3.setHasFixedSize(true);
        mLayoutManagerV3 =new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewV3.setLayoutManager(mLayoutManagerV3);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);


      /*   swiperefresh = (SwipeRefreshLayout) findViewById(R.id.swiperefresh) ;
         swiperefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        swiperefresh.setRefreshing(true);
                        readCategory();


                    }
                }
        );
*/
        readCategory();
       // run();


    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public  void readCategory(){
//        swiperefresh.setRefreshing(true);
     //   progressBar.setVisibility(View.VISIBLE);

        final Map<String,String> params = new HashMap<String, String>();
        params.put("function","readCategory");
        params.put("business_code","1");

        API.POST(this, params, new API.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                progressBar.setVisibility(View.GONE);
          //      swiperefresh.setRefreshing(false);

                try {
                    List<Category> list = new ArrayList<Category>();

                    JSONObject jsonObject = new JSONObject(result);

                    JSONArray array = jsonObject.getJSONArray("data");

                    for(int i =0; i<array.length();i++){
                        JSONObject object2 = array.getJSONObject(i);
                        Category category = new Category(object2.getString("id"),object2.getString("category_id"),object2.getString("category_name"),object2.getString("category_image"),object2.getString("category_description"),object2.getString("business_code"));
                        list.add(category);
                    }

                    setCategory(list);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void readSubCategory(String business_code,String category_id,String categoryName){
        title.setText(categoryName);
       // swiperefresh.setRefreshing(true);
        final Map<String,String> params = new HashMap<String, String>();
        params.put("function","readSubCategory");
        params.put("business_code",business_code);
        params.put("category_id",category_id);

        API.POST(this, params, new API.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
             //   swiperefresh.setRefreshing(false);
                readSalesOrder("1","4455");

                try {
                    List<SubCategory> list = new ArrayList<SubCategory>();

                    JSONObject jsonObject = new JSONObject(result);

                    JSONArray array = jsonObject.getJSONArray("data");

                    for(int i =0; i<array.length();i++){
                        JSONObject object = array.getJSONObject(i);
                        JSONObject object2 = object.getJSONObject("subCategory");
                        JSONObject object3 = object.getJSONObject("price");


                        SubCategory subCategory = new SubCategory(object2.getString("id"),object2.getString("category_id"),object2.getString("sub_category_id"),object2.getString("sub_category_name"),object2.getString("sub_category_image"),
                                 object2.getString("sub_category_description"), object2.getString("business_code"), object3.getString("item_amount"));
                        list.add(subCategory);
                    }

                    setSubCategory(list);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void readSalesOrder(String business_code,String user_code){
       // swiperefresh.setRefreshing(true);
        final Map<String,String> params = new HashMap<String, String>();
        params.put("function","readSalesOrder");
        params.put("business_code",business_code);
        params.put("user_code",user_code);
        params.put("keyword","");

        API.POST(this, params, new API.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
            //    swiperefresh.setRefreshing(false);
                title2.setText("Sales Oders");

                //create order if empty

                try {
                    List<SalesOrder> list = new ArrayList<SalesOrder>();

                    JSONObject jsonObject = new JSONObject(result);

                    if(jsonObject.getBoolean("success")){
                        JSONArray array = jsonObject.getJSONArray("data");

                        for(int i =0; i<array.length();i++){
                            JSONObject object2 = array.getJSONObject(i);

                            SalesOrder salesOrder = new SalesOrder(object2.getString("order_id"),object2.getString("order_number"),object2.getString("order_amount"),object2.getString("payment_status"),object2.getString("user_code"),
                                    object2.getString("clearence_status"), object2.getString("date"), object2.getString("business_code"));
                            list.add(salesOrder);
                        }

                        setSalesOrder(list);
                    }else {

                        recyclerViewV2.removeAllViews();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }


    public void readSales(String order_number,String business_code){
        tvorder.setText(order_number);
        this.order_number = order_number;
      //  swiperefresh.setRefreshing(true);
        final Map<String,String> params = new HashMap<String, String>();
        params.put("function","readSales");
        params.put("business_code",business_code);
        params.put("order_number",order_number);

        API.POST(this, params, new API.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
            //    swiperefresh.setRefreshing(false);
                title3.setText("Sales Oders Items");
                details.setVisibility(View.VISIBLE);
                //create order if empty

                try {
                    List<Sales> list = new ArrayList<Sales>();

                    JSONObject jsonObject = new JSONObject(result);
                    if(jsonObject.getBoolean("success")){
                        title3.setText("Sales Oders Items Total KES."+jsonObject.getString("message"));
                        JSONArray array = jsonObject.getJSONArray("data");

                        StringBuilder sb = new StringBuilder();

                        for(int i =0; i<array.length();i++){
                            JSONObject object1 = array.getJSONObject(i);

                            JSONArray array1 = object1.getJSONArray("subCategory");
                            JSONObject object = array1.getJSONObject(0);
                            JSONObject object3 = object.getJSONObject("subCategory");

                            JSONObject object2 = object1.getJSONObject("sales");
                            Sales sales = new Sales(object2.getString("sales_id"),object2.getString("order_number"),object2.getString("sub_category_id"),object2.getString("item_amount"),object2.getString("item_quantity"),
                                    object2.getString("total_item_amount"), object2.getString("user_code"), object2.getString("date"), object2.getString("business_code"),object3.getString("sub_category_name"));
                            list.add(sales);
                            sb.append(object2.getString("item_quantity")+" "+object3.getString("sub_category_name")+" KES." +object2.getString("total_item_amount")+"\n");
                            food =sb.toString();
                        }
                    }else {
                        recyclerViewV3.removeAllViews();
                    }


                    setSales(list);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void completeOrder(final String order_number) {
        final Map<String,String> params = new HashMap<String, String>();
        params.put("function","completeOrder");
        params.put("order_number",order_number);
        API.POST(this, params, new API.VolleyCallback() {
            @Override
            public void onSuccess(String result) {

                try {
                    JSONObject jsonObject = new JSONObject(result);


                String  dataToPrintCustomer=
                        "\n________________________________\n" +
                        "$big$CASA CAFE\n" +
                        "P.O BOX 61, KABARAK\n" +
                        "TEL: 0703430023\n"+
                         mydate+"\n\n"+
                        "$intro$OrderNo:"+order_number+" Name:"+name.getText().toString().trim()+"\n"+
                        "--------------------------\n"+
                        "Items: \n"+food+"\n" +
                        "Total Amount : "+"KES. "+ jsonObject.getString("data")+"\n" +
                        "--------------------------\nThank you for being our quest.\nPlease come again\n" +
                        "________________________________$intro$$intro$$cut$$intro$ \n";
                if(!phone.getText().toString().matches(""))
                mpesaPayment(jsonObject.getString("data"),order_number,phone.getText().toString());

                sendDataToBTPrinter(dataToPrintCustomer);
                readSalesOrder("1","4455");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void createSalesOrder(String user_code,String business_code){
       // swiperefresh.setRefreshing(true);
        final Map<String,String> params = new HashMap<String, String>();
        params.put("function","createSalesOrder");
        params.put("user_code",user_code);
        params.put("business_code",business_code);

        API.POST(this, params, new API.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
            //    swiperefresh.setRefreshing(false);
                //create order if empty
                try {
                    JSONObject jsonObject = new JSONObject(result);

                   if(jsonObject.getBoolean("success")){
                       readSalesOrder("1","4455");
                   }else {
                       Toast.makeText(MainActivity.this,jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                   }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public void createSales(String sub_category_id,String item_amount,String item_quantity,final String business_code){
       // swiperefresh.setRefreshing(true);
        if(order_number.matches("")){
            Toast.makeText(MainActivity.this,"Select Sales Order or create one",Toast.LENGTH_LONG).show();
         //   swiperefresh.setRefreshing(false);
        }else {
            final Map<String,String> params = new HashMap<String, String>();
            params.put("function","createSales");
            params.put("order_number",order_number);
            params.put("sub_category_id",sub_category_id);
            params.put("item_amount",item_amount);
            params.put("item_quantity",item_quantity);
            params.put("user_code","4455");
            params.put("business_code",business_code);

            API.POST(this, params, new API.VolleyCallback() {
                @Override
                public void onSuccess(String result) {
                  //  swiperefresh.setRefreshing(false);
                    //create order if empty
                    try {
                        JSONObject jsonObject = new JSONObject(result);

                        if(jsonObject.getBoolean("success")){
                            readSales(order_number,business_code);
                        }else {
                            Toast.makeText(MainActivity.this,jsonObject.getString("message"),Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }

    }

    public void setCategory(List<Category> list){
        mAdapterH = new Adapter(MainActivity.this ,list);
        recyclerViewH.setAdapter(mAdapterH);
    }
    public void setSubCategory(List<SubCategory> list){
        mAdapterV1 = new AdapterSubCategory(MainActivity.this ,list);
        recyclerViewV1.setAdapter(mAdapterV1);
    }

    public void setSalesOrder(List<SalesOrder> list){
        mAdapterV2 = new AdapterSalesOrder(MainActivity.this ,list);
        recyclerViewV2.setAdapter(mAdapterV2);
    }

    public void setSales(List<Sales> list){
        mAdapterV3 = new AdapterSales(MainActivity.this ,list);
        recyclerViewV3.setAdapter(mAdapterV3);
    }

    public void sendDataToBTPrinter(String textoToSend) {
        System.out.println("Printing....");
        Intent intentPrint = new Intent();
        intentPrint.setAction(Intent.ACTION_SEND);
        intentPrint.putExtra(Intent.EXTRA_TEXT, textoToSend);
        intentPrint.putExtra("printer_type_id", "4");// For bluetooth
        intentPrint.putExtra("printer_bt_adress", "10:00:E8:6B:E2:5A");
        intentPrint.setType("text/plain");
        System.out.println("sendDataToBTPrinter Start Intent");
        startActivity(intentPrint);
    }



    public void run(){

        final Handler handler = new Handler();
        Timer    timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @SuppressWarnings("unchecked")
                    public void run() {
                        try {
                            readSalesOrderToPrint("1","4455");
                        }
                        catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 5000);
    }

    public void readSalesOrderToPrint(String business_code,String user_code){
       // swiperefresh.setRefreshing(true);
        final Map<String,String> params = new HashMap<String, String>();
        params.put("function","readSalesOrderToPrint");
        params.put("business_code",business_code);
        params.put("user_code",user_code);
        params.put("keyword","");

        API.POST(this, params, new API.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
            //    swiperefresh.setRefreshing(false);
                title2.setText("Sales Oders");

                //create order if empty

                try {
                    List<SalesOrder> list = new ArrayList<SalesOrder>();

                    JSONObject jsonObject = new JSONObject(result);

                    if(jsonObject.getBoolean("success")){
                        JSONArray array = jsonObject.getJSONArray("data");

                        for(int i =0; i<array.length();i++){
                            JSONObject object2 = array.getJSONObject(i);
                            readSalesToPrint(object2.getString("order_number"),"1");
                        }

                    }else {


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }
    public void readSalesToPrint(final String order_number,String business_code){
        tvorder.setText(order_number);
        this.order_number = order_number;
      //  swiperefresh.setRefreshing(true);
        final Map<String,String> params = new HashMap<String, String>();
        params.put("function","readSales");
        params.put("business_code",business_code);
        params.put("order_number",order_number);

        API.POST(this, params, new API.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
              //  swiperefresh.setRefreshing(false);
                title3.setText("Sales Oders Items");
                //create order if empty

                try {
                    List<Sales> list = new ArrayList<Sales>();

                    JSONObject jsonObject = new JSONObject(result);
                    if(jsonObject.getBoolean("success")){
                        JSONArray array = jsonObject.getJSONArray("data");

                        StringBuilder sb = new StringBuilder();

                        for(int i =0; i<array.length();i++){
                            JSONObject object1 = array.getJSONObject(i);

                            JSONArray array1 = object1.getJSONArray("subCategory");
                            JSONObject object = array1.getJSONObject(0);
                            JSONObject object3 = object.getJSONObject("subCategory");

                            JSONObject object2 = object1.getJSONObject("sales");
                            Sales sales = new Sales(object2.getString("sales_id"),object2.getString("order_number"),object2.getString("sub_category_id"),object2.getString("item_amount"),object2.getString("item_quantity"),
                                    object2.getString("total_item_amount"), object2.getString("user_code"), object2.getString("date"), object2.getString("business_code"),object3.getString("sub_category_name"));
                            list.add(sales);
                            sb.append("KES." +object2.getString("item_amount")+" "+object3.getString("sub_category_name")+"\n");
                            food =sb.toString();
                        }

                        String  dataToPrintCustomer="\n________________________________\n" +
                                "$big$CASA CAFE \n" +
                                "$small$Kitchen "+name.getText().toString().trim()+"\n" +
                                "$intro$Order No. : "+order_number+"\n\n" +
                                "Items: \n"+food+"\n" +
                                "Date: "+mydate+" \n "+
                                "________________________________$intro$$intro$$cut$$intro$";

                        sendDataToBTPrinter(dataToPrintCustomer);


                    }else {

                    }


                    setSales(list);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        easyPermission.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onPermissionResult(String permission, boolean isGranted) {
        switch (permission) {
            case Manifest.permission.ACCESS_NETWORK_STATE:
                if (isGranted) {
                    Log.e("readContacts", "granted");
                    //Check internet
                    easyPermission.requestPermission(MainActivity.this, Manifest.permission.INTERNET);

                } else {
                    Log.e("readContacts", "denied");
                    easyPermission.requestPermission(MainActivity.this, Manifest.permission.INTERNET);

                }
                break;
         /*   case android.Manifest.permission.ACCESS_COARSE_LOCATION:
                if (isGranted) {
                    Log.e("readContacts", "granted");
                    //Check internet

                } else {
                    Log.e("readContacts", "denied");
                    easyPermission.requestPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);

                }
                break;
*/

        }
    }
    public void onRadioButtonClicked(View view) {

        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.radio_cash:
                if (checked)
                    phone.setText("");
                    break;
            case R.id.radio_mpesa:
                if (checked)

                    break;
        }
    }

    public void mpesaPayment (String amount,String order_number,String phone_number){
        final Map<String,String> params = new HashMap<String, String>();
        params.put("function","mpesaPayment");
        params.put("business_code",business_code);
        params.put("order_number",order_number);
        params.put("phone_number",phone_number);

        API.POST(this, params, new API.VolleyCallback() {
            @Override
            public void onSuccess(String result) {

            }
        });
    }

}
