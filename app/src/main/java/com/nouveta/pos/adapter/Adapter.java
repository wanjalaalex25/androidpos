package com.nouveta.pos.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nouveta.pos.MainActivity;
import com.nouveta.pos.R;
import com.nouveta.pos.models.Category;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by Alex Boey on 8/1/2016.
 */
public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {



    private Context context;

    private List<Category> mList;

    public Adapter(Context context , List<Category> mList){
        this.context = context;
        this.mList =mList;
    }

    public  class ViewHolder extends RecyclerView.ViewHolder{

        TextView category_name;
       // ImageView category_photo;
        LinearLayout clickable;
        String image = "";
        public ViewHolder(View itemView) {
            super(itemView);
            category_name = (TextView)itemView.findViewById(R.id.category_name);
         //   category_photo = (ImageView) itemView.findViewById(R.id.category_photo);
            clickable = (LinearLayout) itemView.findViewById(R.id.clickable);


        }
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item,parent , false);

        ViewHolder vh = new ViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final Category list= mList.get(position);


        holder.category_name.setText(list.getCategoryName());

      /*  if(list.getCategoryImage().matches("")){
            holder.image ="http://westerndental.ie/wp-content/plugins/social-media-builder//img/no-image.png";
            Picasso.with(context).load("http://westerndental.ie/wp-content/plugins/social-media-builder//img/no-image.png").into(holder.category_photo);
        }else {
            Picasso.with(context).load("http://192.168.0.12:8080/nouveta/POS/images/"+list.getCategoryImage()).into(holder.category_photo);
            holder.image = list.getCategoryImage();
        }
*/
       final String img = holder.image;


        holder.clickable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(context instanceof MainActivity){
                    ((MainActivity)context).readSubCategory(list.getBusinessCode(),list.getCategoryId(),list.getCategoryName());
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }





}
