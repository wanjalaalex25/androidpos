package com.nouveta.pos.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nouveta.pos.MainActivity;
import com.nouveta.pos.R;
import com.nouveta.pos.api.API;
import com.nouveta.pos.models.Sales;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Alex Boey on 8/1/2016.
 */
public class AdapterSales extends RecyclerView.Adapter<AdapterSales.ViewHolder> {



    private Context context;

    private List<Sales> mList;

    public AdapterSales(Context context , List<Sales> mList){
        this.context = context;
        this.mList =mList;
    }

    public  class ViewHolder extends RecyclerView.ViewHolder{

        TextView category_name, item_quantity,minus,add;
      //  ImageView category_photo;
        LinearLayout clickable;
        EditText ed_item_quantity;
        String image = "";
        public ViewHolder(View itemView) {
            super(itemView);
            category_name = (TextView)itemView.findViewById(R.id.category_name);
            item_quantity = (TextView)itemView.findViewById(R.id.item_quantity);
            minus = (TextView)itemView.findViewById(R.id.minus);
            add = (TextView)itemView.findViewById(R.id.add);
         //   category_photo = (ImageView) itemView.findViewById(R.id.category_photo);
            clickable = (LinearLayout) itemView.findViewById(R.id.clickable);
            ed_item_quantity = (EditText) itemView.findViewById(R.id.ed_item_quantity);

        }
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sales,parent , false);

        ViewHolder vh = new ViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final Sales list= mList.get(position);



        holder.category_name.setText(list.getOrderNumber());
        holder.item_quantity.setText(list.getItemQuantity() +" "+list.getSub_category_name() +"   KES "+String.valueOf(getTotal(list.getItemQuantity(),list.getItemAmount())));
        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMinus(list.getSalesId(),String.valueOf(Integer.parseInt(list.getItemQuantity())+1),list.getItemAmount(),list.getOrderNumber());
            }
        });
        holder.ed_item_quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                addMinus(list.getSalesId(), s.toString(),list.getItemAmount(),list.getOrderNumber());

            }
        });
         holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int value = Integer.parseInt(list.getItemQuantity())-1;
                if(value >0){
                    addMinus(list.getSalesId(),String.valueOf(value),list.getItemAmount(),list.getOrderNumber());
                }else {
                    final Map<String,String> params = new HashMap<String, String>();
                    params.put("function","deleteSales");
                    params.put("order_number",list.getOrderNumber());
                    params.put("id",list.getSalesId());
                    API.POST(context, params, new API.VolleyCallback() {
                        @Override
                        public void onSuccess(String result) {
                            try {
                                JSONObject object = new JSONObject(result);
                                Toast.makeText(context,object.getString("message"),Toast.LENGTH_LONG).show();
                                if(context instanceof MainActivity){
                                    ((MainActivity)context).readSales(list.getOrderNumber(),list.getBusinessCode());
                                    ((MainActivity)context).readSalesOrder(list.getBusinessCode(),list.getUserCode());
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            }
        });



    }

    public double getTotal(String value, String value2){

        return Double.parseDouble(value) * Double.parseDouble(value2);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void addMinus(String sales_id ,String item_quantity, String item_amount ,final String orderNumber){
        final Map<String,String> params = new HashMap<String, String>();
        params.put("function","updateSalesItemQuantity");
        params.put("item_quantity",item_quantity);
        params.put("item_amount",item_amount);
        params.put("sales_id",sales_id);
        API.POST(context, params, new API.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                ((MainActivity)context).readSales(orderNumber,MainActivity.business_code);
            }
        });

    }





}
