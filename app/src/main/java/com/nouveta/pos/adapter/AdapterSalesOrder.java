package com.nouveta.pos.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.nouveta.pos.MainActivity;
import com.nouveta.pos.R;
import com.nouveta.pos.models.SalesOrder;
import com.nouveta.pos.models.SubCategory;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by Alex Boey on 8/1/2016.
 */
public class AdapterSalesOrder extends RecyclerView.Adapter<AdapterSalesOrder.ViewHolder> {



    private Context context;

    private List<SalesOrder> mList;
    private RadioButton lastCheckedRB = null;

    public AdapterSalesOrder(Context context , List<SalesOrder> mList){
        this.context = context;
        this.mList =mList;
    }

    public  class ViewHolder extends RecyclerView.ViewHolder{

        RadioButton account_no;
        RadioGroup radioGroup;

        public ViewHolder(View itemView) {
            super(itemView);
            account_no = (RadioButton) itemView.findViewById(R.id.account_no);
            radioGroup = (RadioGroup) itemView.findViewById(R.id.radio);
        }
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_account,parent , false);

        ViewHolder vh = new ViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final SalesOrder list= mList.get(position);
        holder.account_no.setText(list.getOrderNumber());

        holder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                RadioButton checked_rb = (RadioButton) group.findViewById(checkedId);
                if (lastCheckedRB != null) {
                    lastCheckedRB.setChecked(false);
                }
                //store the clicked radiobutton
                lastCheckedRB = checked_rb;

                if(context instanceof MainActivity){
                    ((MainActivity)context).readSales(list.getOrderNumber(),list.getBusinessCode());

                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }





}
