package com.nouveta.pos.api;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.Map;

/**
 * Created by AlexBoey on 5/21/2017.
 */

public class API {

   // public static String URL ="https://mbongocashinternational.tech/mbongo/index.php";
    //public static String URL ="http://169.254.182.107:8080/nouveta/POS/api/v1/index.php";
    public static String URL ="https://pos.nouveta.co.ke/POS/api/v1/index.php";

    public static void POST(final Context context, final Map<String,String> params, final VolleyCallback callback ) {
        RequestQueue queue = Volley.newRequestQueue(context);
        Log.i("# REQUEST",URL);
        Log.i("# PARAMS",params.toString());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String results) {
                        Log.i("# results",results);
                        callback.onSuccess(results);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String,String> getParams(){

                return params;
            }
        };
        queue.add(stringRequest);
    }

    public interface VolleyCallback{
        void onSuccess(String result);
    }

}
